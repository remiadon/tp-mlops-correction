from setuptools import find_packages, setup

with open('requirements.txt') as req_fd:
    install_requires = req_fd.read().splitlines()


setup(
    name="tp-mlops",
    description="TP MLOps",
    version="0.0.1",
    python_requires=">=3.6",
    url="",
    author="MLOps students",
    license="BSD",
    classifiers=[
        "Development Status :: 4 - Beta",
        "License :: OSI Approved :: BSD License",
        "Intended Audience :: Science/Research",
        "Programming Language :: Python :: 3.6",
        "Programming Language :: Python :: 3.7",
        "Programming Language :: Python :: 3.8",
    ],
    packages=find_packages("."),
    install_requires=install_requires,
    #include_dirs=[numpy.get_include()],
)
