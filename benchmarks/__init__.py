import os
import pandas as pd
import joblib

dir_path = os.path.dirname(os.path.realpath(__file__))

def get_model(path):
    model = joblib.load(path)  # pytest metafunc
    return model


def get_dataset(path):
    df = pd.read_csv(path, index_col="PassengerId")
    return df.drop("Survived", axis=1), df["Survived"]
