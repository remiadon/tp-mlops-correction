
from . import get_model, get_dataset
import os
from sklearn.metrics import accuracy_score

dir_path = os.path.dirname(os.path.realpath(__file__))

class Metrics:
    """
    An example benchmark that uses different metrics
    """
    def setup(self):
        self.model = get_model(dir_path + "/../models/titanic.pkl")
        self.X, self.y_true = get_dataset(dir_path + "/../data/test.csv")

    def track_accuracy(self):
        y_pred = self.model.predict(self.X)
        return accuracy_score(y_pred, self.y_true)
